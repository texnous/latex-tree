/**
 * @fileoverview LaTeX syntax tree structure element tests
 * This file is a part of TeXnous project.
 *
 * @copyright TeXnous project team (http://texnous.org) 2016-2017
 * @license LGPL-3.0
 *
 * This unit test is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This unit test is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this unit
 * test; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA.
 */

"use strict";

const Latex = require("@texnous/latex"); // general LaTeX definitions
const LatexSyntax = require("@texnous/latex-syntax"); // LaTeX syntax structures
const LatexTree = require("../../sources/lib/LatexTree"); // LaTeX tree structure elements

let documentBeginCommand = new LatexSyntax.Command({ name: "document" });
let documentEndCommand = new LatexSyntax.Command({ name: "enddocument" });
let documentEnvironment =  new LatexSyntax.Environment({ name: "document" });
let authorCommand = new LatexSyntax.Command({ name: "author", pattern: "[#1]#2", parameters: [{}, {}] });

let
	numberToken,
	wordToken,
	spaceToken,
	paragraphSeparatorToken,
	parameterToken1,
	parameterToken2,
	commandToken,
	environmentBeginCommandToken,
	environmentEndCommandToken,
	environmentBodyToken,
	environmentToken;

/**
 * Tests of LaTeX tree structure elements
 * @author Kirill Chuvilin <kirill.chuvilin@gmail.com>
 */
module.exports = {
	"Token": {
		"constructor": test => {
			test.doesNotThrow(() => numberToken =  new LatexTree.SourceToken({ source: "123", lexeme: Latex.Lexeme.NUMBER }));
			test.doesNotThrow(() => wordToken =	new LatexTree.SourceToken({ source: "Name", lexeme: Latex.Lexeme.WORD }));
			test.doesNotThrow(() => spaceToken = new LatexTree.SpaceToken());
			test.doesNotThrow(() =>
				parameterToken1 =
					new LatexTree.ParameterToken({ hasBrackets: false, hasSpacePrefix: false, childTokens: [ numberToken ]})
			);
			test.doesNotThrow(()  =>
				parameterToken2 =
					new LatexTree.ParameterToken({
						hasBrackets: true,
						hasSpacePrefix: true,
						childTokens: [ wordToken, spaceToken ]
					})
			);
			test.doesNotThrow(() =>
				commandToken =
					new LatexTree.CommandToken({command: authorCommand, childTokens: [ parameterToken1, parameterToken2 ]})
			);
			test.doesNotThrow(() => paragraphSeparatorToken =  new LatexTree.SpaceToken({ lineBreakCount: 2 }));
			test.doesNotThrow(() =>
				environmentBeginCommandToken = new LatexTree.CommandToken({ command: documentBeginCommand })
			);
			test.doesNotThrow(() => environmentEndCommandToken = new LatexTree.CommandToken({ command: documentEndCommand }));
			test.doesNotThrow(() =>
				environmentBodyToken =
					new LatexTree.EnvironmentBodyToken({ childTokens: [ commandToken, paragraphSeparatorToken ]})
			);
			test.doesNotThrow(() =>
				environmentToken = new LatexTree.EnvironmentToken({
					environment: documentEnvironment,
					childTokens: [ environmentBeginCommandToken, environmentBodyToken, environmentEndCommandToken	]
				})
			);
			test.done();
		},
		"insertChildNode": test => {
			test.throws(() => spaceToken.insertChildNode(new LatexTree.ParameterToken({})));
			test.done();
		},
		"toString": test => {
			test.equal(numberToken.toString(true), "LatexTree.SourceToken { \"123\" }");
			test.equal(wordToken.toString(true), "LatexTree.SourceToken { \"Name\" }");
			test.equal(spaceToken.toString(true), "LatexTree.SpaceToken { \" \" }");
			test.equal(paragraphSeparatorToken.toString(true), "LatexTree.SpaceToken { \"\n\n\" }");
			test.equal(parameterToken1.toString(true), "LatexTree.ParameterToken { \"123\" }");
			test.equal(parameterToken2.toString(true), "LatexTree.ParameterToken { \" {Name }\" }");
			test.equal(commandToken.toString(true), "LatexTree.CommandToken { \"\\author[123] {Name }\" }");
			test.equal(environmentBeginCommandToken.toString(true), "LatexTree.CommandToken { \"\\document\" }");
			test.equal(environmentEndCommandToken.toString(true), "LatexTree.CommandToken { \"\\enddocument\" }");
			test.equal(
				environmentBodyToken.toString(true),
				"LatexTree.EnvironmentBodyToken { \"\\author[123] {Name }\n\n\" }"
			);
			test.equal(
				environmentToken.toString(true),
				"LatexTree.EnvironmentToken { \"\\begin{document}\\author[123] {Name }\n\n\\end{document}\" }"
			);
			test.equal(
				environmentToken.toString(),
				"LatexTree.EnvironmentToken [ LatexTree.CommandToken [], LatexTree.EnvironmentBodyToken [ LatexTree.CommandToken [ LatexTree.ParameterToken [ LatexTree.SourceToken [] ], LatexTree.ParameterToken [ LatexTree.SourceToken [], LatexTree.SpaceToken [] ] ], LatexTree.SpaceToken [] ], LatexTree.CommandToken [] ]"
			);
			test.done();
		}
	}
};
